# PerkUp API

PerkUp is customer engagement platform for businesses which helps them increase average customer visitation and spend through customer behaviour tracking.

Website: https://getperkup.com